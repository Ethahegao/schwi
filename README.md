# schwi

## Configuration

Fill the `config` file and rename it `.env`

```sh
cp ./config ./.env
```

## Traefik

Generate password hash

*(you have to double every `$` sign in the result if you put it in a docker-compose.yml file)*

```sh
echo $(htpasswd -nb user password)
```
